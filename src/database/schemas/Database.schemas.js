const { Sequelize, sequelize } = require("..");

//User Schema
const Product = sequelize.define("product", {
  sku: {
    type: Sequelize.STRING,
    unique: true,
  },
  description: {
    type: Sequelize.STRING,
  },
  category: {
    type: Sequelize.STRING,
  },
  price: {
    type: Sequelize.STRING,
  },
  status: {
    type: Sequelize.STRING,
  }
});

module.exports = {
  Product
}
