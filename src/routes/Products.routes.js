//Express
const express = require('express');
const router = express.Router();
//Database Models
const {Product} = require('../database/schemas/Database.schemas');
const axios = require('axios');
const {Op} = require('sequelize');


router.post('/product', async (request, response) => {
  try {
    const {sku, description, categoryKey, price} = request.body;

          product = await Product.create({
            sku,
            description,
            price,
            status : "actived"
          });
          return response.status(200).json({
          status: 200,
          data: product,
          meta: null,
          message: 'The product was sucessfully registered in the table',
    });

  } catch (error) {    
      return response.status(500).json({
        status: 500,
        message: error.message || error,
        data: [],
        meta: null,
      });
  }
});

router.get("/all-products", async (request, response) => {
  try {
    const products = await Product.findAll();
    return response.status(200).json({
      status: 200,
      data: products,
      meta: null,
      message: 'Sucess',
    });
  } catch (error) {
    console.error(error);
    response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

router.get('/products-filtered/', async (request, response) => {
  try {
    const description = request.query.description
    const category = request.query.category
    
    if (!request.query.description && !request.query.category) {
      return response.status(400).json({
        status: 400,
        data: [],
        meta: null,
        message: 'Pass at least one parameter to the filter description ot category',
      });}

    if (request.query.description && !request.query.category) {
      const products = await Product.findAndCountAll({
        where: {
          description: {[Op.like]: `%${description}%`},
        },
        raw: true,
      });

      if (!products) {
      response.status(400).json({
        status: 400,
        data: [],
        meta: null,
        message: `There is none product to this filter`,
      });  
    }
      return response.status(200).json({
          status: 200,
          data: products,
          meta: null,
          message: "Were found the following products to the filter",
        });
    
    }

    if (!request.query.description && request.query.category) {
       const products = await Product.findAndCountAll({
        where: {
          category: {[Op.like]: `%${category}%`},
        },
        raw: true,
      });
    if (!products) {
      response.status(400).json({
        status: 400,
        data: [],
        meta: null,
        message: `There is none product to this filter`,
      });  
    }
    return response.status(200).json({
          status: 200,
          data: products,
          meta: null,
          message: "Were found the following products to the filter",
        });
    }  
      
    if (request.query.description && request.query.category) {
       const products = await Product.findAndCountAll({
        where: {
          category: {[Op.like]: `%${category}%`} ,
          description: {[Op.like]: `%${description}%`} ,
        },
        raw: true,
      });
    if (!products) {
      response.status(400).json({
        status: 400,
        data: [],
        meta: null,
        message: `There is none product to this filter`,
      });  
    }
    return response.status(200).json({
          status: 200,
          data: products,
          meta: null,
          message: "Were found the following products to the filter",
        });
    }
        
       
  } catch (error) {
    console.error(error);
    return response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

router.put("/product/insert-category",  async (request, response) => {
  try {
    const operationStatus = await Product.update({
      category: request.body.category },
      {where: { sku: request.body.sku }},
    );
    if (operationStatus[0] === 1) {
      response.status(200).json({
        status: 200,
        data: [],
        meta: null,
        message: `Product ${request.body.sku} was successfully updated.`,
      });
    } else {
      response.status(404).json({
        status: 404,
        data: [],
        meta: null,
        message: `Could not find the product ${request.body.sku}`,
      });
    }
  } catch (error) {
    console.error(error);
    response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

router.put("/product/disable",  async (request, response) => {
  try {
    const operationStatus = await Product.update({
      status: "disabled" },
      {where: { sku: request.body.sku }},
    );
    if (operationStatus[0] === 1) {
      response.status(200).json({
        status: 200,
        data: [],
        meta: null,
        message: `Product ${request.body.sku} was successfully disabled.`,
      });
    } else {
      response.status(404).json({
        status: 404,
        data: [],
        meta: null,
        message: `Could not find the product ${request.body.sku}`,
      });
    }
  } catch (error) {
    console.error(error);
    response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

router.put("/product/update-data",  async (request, response) => {
  try {
    const operationStatus = await Product.update({
      category: request.body.category },
      {where: { sku: request.body.sku }},
    );
    if (operationStatus[0] === 1) {
      response.status(200).json({
        status: 200,
        data: [],
        meta: null,
        message: `Product ${request.body.sku} was successfully updated.`,
      });
    } else {
      response.status(404).json({
        status: 404,
        data: [],
        meta: null,
        message: `Could not find the product ${request.body.sku}`,
      });
    }
  } catch (error) {
    console.error(error);
    response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

router.delete("/product",  async (request, response) => {
  try {
    const operationStatus = await Product.destroy({
      where: { sku: request.body.sku },
    });
    if (operationStatus === 1) {
      response.status(200).json({
        status: 200,
        data: [],
        meta: null,
        message: `Product ${request.body.sku} was successfully deleted from products table.`,
      });
    } else {
      response.status(404).json({
        status: 404,
        data: [],
        meta: null,
        message: `Could not find the product in the database ${request.body.sku}`,
      });
    }
  } catch (error) {
    console.error(error);
    response.status(500).json({
      status: 500,
      message: error.message || error,
      data: [],
      meta: null,
    });
  }
});

module.exports = router;
