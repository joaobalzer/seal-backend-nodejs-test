# SEAL API V1.0.0

## Installing the project

- Clone the project to you local machine or server;
- Open the root diretory;
- Make sure you have NodeJS, NPM, and YARN installed in your enviroment;
- Create a file on root diretory, called `.env`;
- The `.env` file contains private variables that must not be exposed on the web for security purposes. **You might get these keys with a system administrator**;
- Run `yarn install`;
- Run `yarn dev` to development mode (_default on port 8080_);

> Disclaimer of liability: this version do not have production configurations yet; I do not recommend using this version of the project for production purposes.

### API Documentation

> [Click here](https://www.getpostman.com/collections/c2767aabf8a58b734aee) to reach the Postman's collection JSON.

> You may also use postman's files that are located at the project's root, to load a local and interactive collection.
